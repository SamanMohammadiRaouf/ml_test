# gcp-cc-development

## Requirements

- Python 3.9 (tested under Python 3.9.12)
- `pip install -r cloud_function/requirements`

## Local Test using `subscriber.py`

1. Copy `key.json` (gcp api key) in the root directory
2. ‍`cd cloud_function`
3. Test each function:
    - test ingestion function: `python subscribe.py ingestion_function`
    - test activator function: `python subscribe.py activator_function`
    - test decision function: `python subscribe.py decision_function`


## Local Test using `pytest`

1. Copy `key.json` (gcp api key) in the root directory
2. ‍`cd cloud_function`
3. Test all functions: `pytest`

## Retreive GCP Logging

1. ‍`cd cloud_function`
2. `python retreive_logging.py`

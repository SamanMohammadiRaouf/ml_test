import dataclasses
import json
from datetime import datetime as dt
import os
import base64

from google.cloud import bigquery ## Do not remove this!!
from google.cloud import pubsub_v1

from utils.dataclasses.visit_detail import *
from utils.logger import logger
from ingestion.parsers.form_interaction import *
from ingestion.parsers.page_view import *
from ingestion.parsers.session import *


# Publishes a message to a Cloud Pub/Sub topic.
def ingestion_function(event, context):

    logger.debug(f'ingestion function called.')

    PUBSUB_CC_RAW_DATA = os.getenv('PUBSUB_CC_RAW_DATA')
    GOOGLE_PROJECT_ID = os.getenv('GOOGLE_PROJECT_ID')

    event_data = event['data']
    try:
        event_data = base64.b64decode(event_data).decode('utf-8')
    except Exception as e:
        logger.warning(e)

    logger.info(f'decision function event loaded. event_data={event_data}')

    if event_data=="FormAnalytics publish" or event_data=="Matomo publish":
        logger.info("return from ingestion function: the event_data has not proper content")
        return

    data = {'body': json.loads(event_data),
            'userAgent': json.loads(event['attributes']['UserAgent']),
            'time': json.loads(event['attributes']['time']),
            'ip': json.loads(event['attributes']['ip'])}

    logger.info("ingestion data: ", data)

    cc_raw_data = None
    visitDetail = VisitDetail()
    logger.debug("parsing session...")
    session = session_parser(data)
    logger.info(f"session parsed: {session}")
    visitDetail.session = session
    visitDetail.parseTime = str(dt.utcnow())
    visitDetail.requestDate = data['time']
    visitDetail.pageviewid = data['body']['pv_id'] if 'pv_id' in data['body'].keys() else None
    
    if 'action_name' in data['body'].keys():
        try:
            logger.debug('parsing pageview...')
            cc_raw_data = page_view_parser(data)
            visitDetail.Pageview = cc_raw_data
            logger.info("pageview parsed: ", dataclasses.asdict(cc_raw_data))
        except Exception as e:
            logger.error(f"pageview parsing error: {e}")

    if 'fa_fields' in data['body'].keys():
        logger.debug('parsing form interaction...')
        cc_raw_data = form_interaction_parser(data)
        logger.info(f'form interaction parsed:{cc_raw_data}')
        visitDetail.FormInteraction = cc_raw_data

    if 'e_c' in data['body'].keys():
        pass
    
    if not visitDetail:
        logger.info('return from ingestion function: visitDetail is null!')
        return

    # Instantiates a Pub/Sub client
    publisher = pubsub_v1.PublisherClient()
    topic_path = publisher.topic_path(GOOGLE_PROJECT_ID, PUBSUB_CC_RAW_DATA)

    message_json = json.dumps({
        'data': dataclasses.asdict(visitDetail),
    })
    message_bytes = message_json.encode('utf-8')
    # Publishes a message
    try:
        logger.debug("publishing the message...")
        publish_future = publisher.publish(topic_path, data=message_bytes)
        publish_future.result()  # Verify the publish succeeded
        logger.info(f"message published to {PUBSUB_CC_RAW_DATA}:{message_json}")
        return 'Message published.'
    except Exception as e:
        logger.error(e)
        return (e, 500)

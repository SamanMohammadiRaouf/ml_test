import datetime
from decimal import Decimal
from ip2geotools.databases.noncommercial import DbIpCity
from user_agents import parse
from utils.logger import logger

from utils.dataclasses.visit_detail import *

def session_parser(data):
    b = data['body']
    logger.debug("getting DbIpCity...")
    ipresponse = DbIpCity.get(data['ip'], api_key='free')
    logger.info(f"ip info: {ipresponse}".replace("\n",", "))
    user_agent = parse(data['userAgent'])
    user_agent = str(user_agent)
    user_agent = user_agent.split(" / ")
    ## timezone calculation
    reqdate = datetime.combine(datetime.today(), datetime.strptime(data['time'],"%H:%M:%S").time())
    delta = (reqdate - datetime.utcnow()).total_seconds()/3600
    if delta >= 12:
        delta = delta-24
    elif delta <= -12:
        delta = delta+24
    delta = round(delta, 1)
    tz = delta if Decimal(delta) % Decimal(0.5) == Decimal(0.0) else ''

    session = Session(siteId=b['idsite'] if 'idsite' in b.keys() else None,
                        visitorId=b['_id'] if '_id' in b.keys() else None,
                        fingerprint=b['_uid'] if '_uid' in b.keys() else None,
                        ipLocation=IpLocation(ip=data['ip'],
                                              city=ipresponse.city,
                                              country=ipresponse.country,
                                              region=ipresponse.region,
                                              latitude=ipresponse.latitude,
                                              longitude=ipresponse.longitude),
                        runtimeSpec=RuntimeSpec(timeZone=tz,
                                                resolution=b['res'] if 'res' in b.keys() else None,
                                                cookie=bool(int(b['cookie'])) if 'cookie' in b.keys() else False,
                                                flash=bool(int(b['fla'])) if 'fla' in b.keys() else False,
                                                java=bool(int(b['java'])) if 'java' in b.keys() else False,
                                                pdf=bool(int(b['pdf'])) if 'pdf' in b.keys() else False,
                                                quicktime=bool(int(b['qt'])) if 'qt' in b.keys() else False,
                                                realplayer=bool(int(b['realp'])) if 'realp' in b.keys() else False,
                                                silverlight=bool(int(b['ag'])) if 'ag' in b.keys() else False,
                                                windowsmedia=bool(int(b['wma'])) if 'wma' in b.keys() else False),
                        userAgent=UserAgent(rawString=data['userAgent'],
                                            browser=Browser(name=user_agent[2]),
                                            device=DeviceInfo(model=user_agent[0]),
                                            os=OS(name=user_agent[1])))

    return session
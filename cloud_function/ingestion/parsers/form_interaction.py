import json

from utils.logger import logger
from utils.dataclasses.visit_detail import *


def get_start_field(b):
    if 'fa_st' in b:
        if b['fa_st'] == '1':
            return True
    return False

def get_issubmit_field(b):
    if 'fa_su' in b:
        if b['fa_su'] == '1':
            return True
    return False

def form_interaction_parser(data):
    b = data['body']
    try:
        f_list = json.loads(b['fa_fields']) if 'fa_fields' in b.keys() else None
    except Exception as e:
        logger.error(e)
        f_list = None

    form_interaction = FormInteraction(
                                       entryField=b['fa_ef'] if 'fa_ef' in b.keys() else None,
                                       exitField=b['fa_lf'] if 'fa_lf' in b.keys() else None,
                                       start= get_start_field(b),
                                       isSubmit=get_issubmit_field(b),
                                       hesitationTime=b['fa_ht'] if 'fa_ht' in b.keys() else None,
                                       timeSpent=b['fa_ts'] if 'fa_ts' in b.keys() else None,
                                       timeToSubmission=b['fa_tts'] if 'fa_tts' in b.keys() else None,
                                       formfields=[FormField(name=f['fa_fn'] if 'fa_fn' in f.keys() else None,
                                                             content=f['fa_cn'] if 'fa_cn' in f.keys() else None,
                                                             size=f['fa_fs'] if 'fa_fs' in f.keys() else None,
                                                             leftBlank=f['fa_fb'] if 'fa_fb' in f.keys() else None,
                                                             submitted=f['fa_su'] if 'fa_su' in f.keys() else False,
                                                             hesitationTime=f['fa_fht'] if 'fa_fht' in f.keys() else None,
                                                             timeSpent=f['fa_fts'] if 'fa_fts' in f.keys() else None,
                                                             numChange=f['fa_fch'] if 'fa_fch' in f.keys() else None,
                                                             numFocus=f['fa_ff'] if 'fa_ff' in f.keys() else None,
                                                             numDel=f['fa_fd'] if 'fa_fd' in f.keys() else None,
                                                             numCursor=f['fa_fcu'] if 'fa_fcu' in f.keys() else None)
                                                   for f in f_list] if f_list else None,
                                       formview=Formview(formviewid=b['fa_vid'] if 'fa_vid' in b.keys() else None,
                                                         formName=b['fa_id'] if 'fa_id' in b.keys() else None),
                                        )

    return form_interaction
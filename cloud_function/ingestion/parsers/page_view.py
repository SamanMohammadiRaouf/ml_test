from utils.dataclasses.visit_detail import *

def page_view_parser(data):
    b = data['body']
    page_view = Pageview(
                         page=Page(
                                  #  siteId=b['idsite'],
                                   name=b['action_name'],
                                   url=b['url'],
                                #    firstRequestDate=data['time']
                                   ),
                         refPageUrl=b['urlref'] if 'urlref' in b else None,
                         formviews=[Formview(
                                            # requestDate=data['time'],
                                             formviewid=fa_fp_i['fa_vid'],
                                             formName=fa_fp_i['fa_id'] if 'fa_id' in fa_fp_i.keys() else None,
                                            #  form=Form(siteId=b['idsite'],
                                            #            name=fa_fp_i['fa_id'] if 'fa_id' in fa_fp_i.keys() else None,
                                            #            firstRequestDate=data['time']
                                            #            ),
                                            #  pageview=Pageview(pageviewid=b['pv_id'])
                                             )
                                    for fa_fp_i in b['fa_fp']] if 'fa_fp' in b.keys() else None)

    return page_view
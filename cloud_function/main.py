from decision.main import decision_function
from ingestion.main import ingestion_function
from activator.main import activator_function
from ps2bq.main import ps2bq_function


def decision_main(event, context):
    decision_function(event, context)

def ingestion_main(event, context):
    ingestion_function(event, context)

def activator_main(event, context):
    activator_function(event, context)

def ps2bq_main(event, context):
    ps2bq_function(event, context)

import os
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')

os.environ['GENERAL_LOG_LEVEL']                 = config.get('general', 'GENERAL_LOG_LEVEL')
os.environ['GENERAL_LOG_TO_CLOUD']              = config.get('general', 'GENERAL_LOG_TO_CLOUD')
os.environ['GOOGLE_APPLICATION_CREDENTIALS']    = config.get('google',  'GOOGLE_APPLICATION_CREDENTIALS')
os.environ['GOOGLE_PROJECT_ID']                 = config.get('google',  'GOOGLE_PROJECT_ID')
os.environ['PUBSUB_DECISION_TRIGGER']           = config.get('pubsub',  'PUBSUB_DECISION_TRIGGER')
os.environ['PUBSUB_CC_RAW_DATA']                = config.get('pubsub',  'PUBSUB_CC_RAW_DATA')
os.environ['BIGQUERY_DATASET_NAME']             = config.get('bigquery','BIGQUERY_DATASET_NAME')
os.environ['BIGQUERY_TABLE_NAME']               = config.get('bigquery','BIGQUERY_TABLE_NAME')
os.environ['API_URL']                           = config.get('api',     'API_URL')

GOOGLE_PROJECT_ID       = config.get('google', 'GOOGLE_PROJECT_ID')
PUBSUB_CC_RAW_DATA      = config.get('pubsub', 'PUBSUB_CC_RAW_DATA')
PUBSUB_DECISION_TRIGGER = config.get('pubsub', 'PUBSUB_DECISION_TRIGGER')
PUBSUB_MATOMO_RAW_DATA  = config.get('pubsub', 'PUBSUB_MATOMO_RAW_DATA')

#####################################################################

import json
import sys
from concurrent.futures import TimeoutError
from google.cloud import bigquery ## Do not remove this!!
from google.cloud import pubsub_v1

from decision.main import decision_function
from ingestion.main import ingestion_function
from activator.main import activator_function

function = {
    'decision_function': decision_function,
    'ingestion_function': ingestion_function,
    'activator_function': activator_function
}

topic = {
    'decision_function': f'{PUBSUB_DECISION_TRIGGER}-sub',
    'ingestion_function': f'{PUBSUB_MATOMO_RAW_DATA}-sub',
    'activator_function': f'{PUBSUB_CC_RAW_DATA}-sub'
}

if __name__=='__main__':

    function_name = sys.argv[1]
    subscription_id = topic[function_name]
    main_function = function[function_name]

    # Number of seconds the subscriber should listen for messages
    timeout = 1000
    subscriber = pubsub_v1.SubscriberClient()
    subscription_path = subscriber.subscription_path(GOOGLE_PROJECT_ID, subscription_id)


    def callback(message: pubsub_v1.subscriber.message.Message) -> None:
        data_str = message.data.decode("utf-8")
        data_dict = json.loads(data_str)
        attributes_dict = dict(message.attributes)
        message_dict = {'data': json.dumps(data_dict), 'attributes': attributes_dict}
        message_str = json.dumps(message_dict)
        print(f'input message: {message_str}')
        main_function(message_dict, None)
        message.ack()


    streaming_pull_future = subscriber.subscribe(subscription_path, callback=callback)
    print(f"Listening for messages on {subscription_path}..\n")

    # Wrap subscriber in a 'with' block to automatically call close() when done.
    with subscriber:
        try:
            # When `timeout` is not set, result() will block indefinitely,
            # unless an exception is encountered first.
            streaming_pull_future.result(timeout=timeout)
        except TimeoutError:
            streaming_pull_future.cancel()  # Trigger the shutdown.
            streaming_pull_future.result()  # Block until the shutdown is complete.

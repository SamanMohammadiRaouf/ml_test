from google.cloud import bigquery ## Do not remove this!!
from google.cloud import pubsub_v1
import dataclasses
import json
from utils.dataclasses.make_form_decision import *
from utils.dataclasses.decision import *
from utils.enums.restriction_policy import *
from utils.enums.service_name import *
from utils.logger import logger
import os
import base64


def extract_email(formfields):
    for field in formfields:
        if 'email' in field['name']:
            return field['content']
    else:
        return None

def extract_fraud_type(form_name):
    if 'login' in form_name:
        return ServiceName.TAKEOVER
    else:
        return ServiceName.OPENING

def activator_function(event, context):

    logger.debug(f'activator function called.')

    PUBSUB_DECISION_TRIGGER = os.getenv('PUBSUB_DECISION_TRIGGER')
    GOOGLE_PROJECT_ID = os.getenv('GOOGLE_PROJECT_ID')

    event_data = event['data']
    try:
        event_data = base64.b64decode(event_data).decode('utf-8')
    except Exception as e:
        logger.warning(e)

    cc_raw_data = json.loads(event_data)['data']
    logger.info(f'activator function event loaded. cc_raw_data={cc_raw_data}')

    if not 'FormInteraction' in cc_raw_data.keys():
        return
    if not cc_raw_data['FormInteraction']:
        return
    is_submit = cc_raw_data['FormInteraction']['isSubmit']
    logger.info(f"is_submit: {is_submit}")
    if not is_submit:
        return
    try:
        make_form_decision = MakeFormDecision(
            account=Account(email=extract_email(cc_raw_data['FormInteraction']['formfields'])),
            device=Device(fingerprint=cc_raw_data['session']['fingerprint']),
            siteId=cc_raw_data['session']['siteId'],
            formviewid=cc_raw_data['FormInteraction']['formview']['formviewid'],
            serviceName=extract_fraud_type(cc_raw_data['FormInteraction']['formview']['formName']),
            isBlocked=None,
            firstSeenBySystem=True
        )

        make_form_decision_json = json.dumps({
                        'data': dataclasses.asdict(make_form_decision),
                    })
        # Instantiates a Pub/Sub client
        publisher = pubsub_v1.PublisherClient()
        topic_path = publisher.topic_path(GOOGLE_PROJECT_ID, PUBSUB_DECISION_TRIGGER)
        message_bytes = make_form_decision_json.encode('utf-8')
        logger.debug("publishing the message...")
        publish_future = publisher.publish(topic_path, data=message_bytes)
        publish_future.result()  # Verify the publish succeeded
        logger.info(f"message published to {PUBSUB_DECISION_TRIGGER}:{make_form_decision_json}")
        return 'Message published.'
    except Exception as e:
        logger.error(e)
        return (e, 500)

from dataclasses import dataclass
from dataclasses_json import dataclass_json

from utils.dataclasses.decision import Account
from utils.dataclasses.decision import Device

@dataclass_json
@dataclass
class decisionDetails:
    decisionOnDevice: str = None
    decisionOnAccount: str = None

@dataclass_json
@dataclass
class MakeFormDecision:
    _id: str = None
    projectId: str = None
    siteId: int = None
    account: Account = None
    device: Device = None
    formviewid: str = None
    serviceName: str = None
    isBlocked: bool = None
    decisionDetails: decisionDetails = None
    firstSeenBySystem: bool = None
    _created: str = None
    _updated: str = None
    _etag: str = None
from dataclasses import dataclass
from datetime import datetime
from dataclasses_json import dataclass_json


@dataclass_json
@dataclass
class IpLocation:
    ip: bytes = None
    city: str = None
    country: str = None
    latitude: float = None
    longitude: float = None
    region: str = None

@dataclass_json
@dataclass
class RuntimeSpec:
    timeZone: str = None
    resolution: str = None
    cookie: bool = None
    flash: bool = None
    java: bool = None
    pdf: bool = None
    quicktime: bool = None
    realplayer: bool = None
    silverlight: bool = None
    windowsmedia: bool = None

@dataclass_json
@dataclass
class Browser:
    lang: str = None
    engine: str = None
    name: str = None
    version: str = None

@dataclass_json
@dataclass
class DeviceInfo:
    brand: str = None
    model: str = None
    type: int = None

@dataclass_json
@dataclass
class OS:
    name: str = None
    version: str = None

@dataclass_json
@dataclass
class UserAgent:
    rawString: str = None
    browser: Browser = None
    device: DeviceInfo = None
    os: OS = None

@dataclass_json
@dataclass
class Session:
    siteId: int = None
    visitorId: str = None
    fingerprint: str = None
    ipLocation: IpLocation = None
    runtimeSpec: RuntimeSpec = None
    userAgent: UserAgent = None

@dataclass_json
@dataclass
class Page:
    name: str = None
    url: str = None

@dataclass_json
@dataclass
class Pageview:
    page: Page = None
    refPageUrl: str = None
    formviews: list = None

@dataclass_json
@dataclass
class Formview:
    formviewid: str = None
    formName: str = None

@dataclass_json
@dataclass
class FormField:
    name: str = None
    content: str = None
    size: int = None
    leftBlank: bool = None
    submitted: bool = None
    hesitationTime: int = None
    timeSpent: int = None
    numChange: int = None
    numFocus: int = None
    numDel: int = None
    numCursor: int = None

@dataclass_json
@dataclass
class FormInteraction:
    entryField: str = None
    exitField: str = None
    start: bool = None
    isSubmit: bool = None
    hesitationTime: int = None
    timeSpent: int = None
    timeToSubmission: int = None
    formfields: list = None
    formview: Formview = None

@dataclass_json
@dataclass
class VisitDetail:
    parseTime: datetime = None # save string in it!
    requestDate: str = None
    pageviewid: str = None
    session: Session = None
    Pageview: Pageview = None
    FormInteraction: FormInteraction = None
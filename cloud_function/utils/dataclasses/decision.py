from dataclasses import dataclass
from dataclasses_json import dataclass_json

@dataclass_json
@dataclass
class Device:
    fingerprint: str = None

@dataclass_json
@dataclass
class Account:
    email: str = None

@dataclass_json
@dataclass
class Rule:
    isBlocked: bool = None
    exceptions: list = None

@dataclass_json
@dataclass
class AutomaticDecisionDetail:
    isManualDecisionRequired: bool = None
    makeFormDecisionId: str = None
    score: int = None
    signals: list = None
    fraudServicesId: str = None

@dataclass_json
@dataclass
class Decision:
    _id: str = None
    serviceName: str = None
    projectId: str = None
    account: Account = None
    device: Device = None
    rule: Rule = None
    automaticDecisionDetail: AutomaticDecisionDetail = None
from dataclasses import dataclass
from dataclasses_json import dataclass_json
from utils.enums.service_name import *

@dataclass_json
@dataclass
class ManualDecisionRequiredAt:
    fromScore: int = None
    toScore: int = None

@dataclass_json
@dataclass
class AutomaticDecision:
    blockFromScore: int = None
    accountRestrictionPolicy: str = None #block only current risky device",
    deviceRestrictionPolicy: str = None #block only current risky account

@dataclass_json
@dataclass
class FraudService:
    _id: str = None
    sensitivityLevel: str = None
    serviceName: str = ServiceName.TAKEOVER
    projectId: str = None
    #sensitivityLevel: str = None # low risk
    manualDecisionRequiredAt: ManualDecisionRequiredAt = None
    automaticDecision: AutomaticDecision = None


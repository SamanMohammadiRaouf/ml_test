import os
import logging
import google.cloud.logging # Don't conflict with standard logging
from google.cloud.logging.handlers import CloudLoggingHandler, setup_logging

client = google.cloud.logging.Client()

logger = logging.getLogger('cloudLogger')

GENERAL_LOG_LEVEL = os.getenv('GENERAL_LOG_LEVEL')
GENERAL_LOG_TO_CLOUD = os.getenv('GENERAL_LOG_TO_CLOUD')
logger.setLevel(logging._nameToLevel[GENERAL_LOG_LEVEL])

if GENERAL_LOG_TO_CLOUD == "True":
    handler = CloudLoggingHandler(client)
    setup_logging(handler)
else:
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    handler.setFormatter(formatter)
    logger.addHandler(handler)
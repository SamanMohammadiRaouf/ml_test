class FF:
    NUM_LOGIN_ATTEMPTS_ON_EMAIL = 'num_login_attempts_on_email'
    NUM_SIGNUP_ATTEMPTS_ON_EMAIL = 'num_signup_attempts_on_email'

    NUM_CITIES_ON_EMAIL = 'num_cities_on_email'
    NUM_IP_ON_EMAIL = 'num_ip_on_email'
    NUM_COUNTRIES_ON_EMAIL = 'num_countries_on_email'
    NUM_DEVICES_ON_EMAIL = 'num_device_on_email'
    NUM_OS_ON_EMAIL = 'num_OS_on_email'

    NUM_EMAIL_ON_IPS_ON_EMAIL = 'num_email_on_ips'
    NUM_EMAIL_ON_IP = 'num_email_on_curr_ip'

    FORM_TIME_SPENT = 'form_time_spent'
    IS_EMAIL_DOMAIN_PROVIDER_FREE = 'is_email_domain_provider_free'

from google.cloud import bigquery
import base64, json, os
from utils.logger import logger

def ps2bq_function(event, context):
   logger.debug(f'ps2bq function called.')

   BIGQUERY_DATASET_NAME = os.getenv('BIGQUERY_DATASET_NAME')
   BIGQUERY_TABLE_NAME = os.environ['BIGQUERY_TABLE_NAME']

   event_data = event['data']
   try:
      event_data = base64.b64decode(event_data).decode('utf-8')
   except Exception as e:
      logger.warning(e)
   
   data_json = json.loads(event_data)
   logger.info(f'ps2bq function event loaded. data_json={data_json}')

   to_bigquery(BIGQUERY_DATASET_NAME, BIGQUERY_TABLE_NAME, data_json)

def to_bigquery(dataset, table, document):
   bigquery_client = bigquery.Client()
   dataset_ref = bigquery_client.dataset(dataset)
   table_ref = dataset_ref.table(table)
   table = bigquery_client.get_table(table_ref)
   errors = bigquery_client.insert_rows(table, [document])
   if len(errors) == 0:
      logger.info(f'record added to bigquery: {document}')
   else:
      logger.error(f'record NOT added to bigquery: {errors}')
import requests
import json

from utils.logger import logger
from utils.dataclasses.decision import *
from utils.dataclasses.make_form_decision import *
from utils.dataclasses.fraud_services import *


class API:
    def __init__(self, url):
        self.url = url
    
    def get_projectId_by_siteId(self, siteId):
        url = f'{self.url}/app2sites?where={{"siteId":{siteId}}}&max_results=1'
        logger.debug(f"getting app2sites... url={url}")
        response = requests.get(url)
        response.raise_for_status()
        item_list = response.json()['_items']
        if len(item_list)==0:
            logger.info(f"app2sites get result: no site (siteId={siteId})")
            return None
        item_dict = item_list[0]
        project_id = item_dict['projectId']
        logger.info(f'app2sites get result: projectId={project_id}')
        return project_id


    def post_decision(self, decision: Decision):
        url = f'{self.url}/projects/{decision.projectId}/fraudServices/{decision.serviceName}/decisionArchives'
        decision_without_null = {k: v for k, v in decision.to_dict().items() if v}
        logger.debug(f"posting decision... body={json.dumps(decision_without_null)}, url={url}")
        response = requests.post(url, json=decision_without_null)
        response.raise_for_status()
        logger.info(f'decision posted: {decision.account.email if decision.account else decision.device.fingerprint}')
        # decision._id = response.json()['_id']
        # return decision #TODO: uncomment
        return self.get_decision(
            project_id=decision.projectId, 
            service_name=decision.serviceName, 
            account=decision.account,
            device=decision.device)

    def post_make_form_decision(self, make_form_decision: MakeFormDecision):
        # The URL to send the request to
        url = f'{self.url}/projects/{make_form_decision.projectId}/fraudServices/{make_form_decision.serviceName}/makeFormDecision'
        # Process the request
        make_form_decision_dict = {
            "account": {"email": make_form_decision.account.email},
            "device": {"fingerprint": make_form_decision.device.fingerprint},
            "formviewid": make_form_decision.formviewid,
            "isBlocked": make_form_decision.isBlocked,
            "firstSeenBySystem": make_form_decision.firstSeenBySystem
        }
        make_form_decision_dict = {k: v for k, v in make_form_decision_dict.items() if v}
        logger.debug(f"posting makeFormDecision... body={json.dumps(make_form_decision_dict)}, url={url}")
        response = requests.post(url, json=make_form_decision_dict)
        response.raise_for_status()
        response_json = response.json()
        # if response_json['_status'] == 'ERR':
        #     response_json = response_json['_error']['message']
        # logger.info(f"keys: {response_json.keys()}")
        # logger.info(f"response: {response_json}")
        make_form_decision._id = response_json['_id']
        make_form_decision._etag = response_json['_etag']
        logger.info(f'makeFormDecision posted: email={make_form_decision.account.email}, id={make_form_decision._id}')
        return make_form_decision

    def patch_make_form_decision(self, make_form_decision: MakeFormDecision):
        # The URL to send the request to
        url = f'{self.url}/projects/{make_form_decision.projectId}/fraudServices/{make_form_decision.serviceName}/makeFormDecision/{make_form_decision._id}'
        # Process the request
        make_form_decision_dict = {
            "isBlocked": make_form_decision.isBlocked,
            "decisionDetails": {
                "decisionOnDevice": {
                    "decisionId": make_form_decision.decisionDetails.decisionOnDevice
                },
                "decisionOnAccount": {
                    "decisionId": make_form_decision.decisionDetails.decisionOnAccount
                }
            },
            "device":{
                "fingerprint": make_form_decision.device.fingerprint
            }
        }
        # response = requests.get(url)
        # etag = response.json()['_etag']
        logger.debug(f"patching makeFormDecision... body={json.dumps(make_form_decision_dict)}, url={url}, _etag={make_form_decision._etag}")
        response = requests.patch(url, json=make_form_decision_dict, headers={'If-Match':make_form_decision._etag})
        try:
            response.raise_for_status()
            logger.info(f'makeFormDecision patched: email={make_form_decision.account.email}, isBlocked={make_form_decision.isBlocked}')
        except Exception as e:
            logger.error(f'makeFormDecision patch failed: email={make_form_decision.account.email}, _id={make_form_decision._id}, exception={e}, response={response.json()}')
        return make_form_decision
    
    def get_decision(self, project_id, service_name, account = None, device = None):
        if account:
            where = f'"account.email":"{account.email}"'
        if device:
            where = f'"device.fingerprint":"{device.fingerprint}"'

        if not where:
            logger.info(f'decision get result: where condition is empty. account={account}, device={device}')
            return None

        url = f'{self.url}/projects/{project_id}/fraudServices/{service_name}/decisionArchives?where={{{where}}}&sort=-_created&max_results=1' #TODO: decisionArchives
        # Process the request
        logger.debug(f"getting decision... url={url}")
        response = requests.get(url)
        response.raise_for_status()
        decision_list = response.json()['_items']
        if len(decision_list)==0:
            logger.info(f'decision get result: no decision found. url={url}')
            return None
        decision_dict = decision_list[0]
        # del decision_dict['_updated']
        # del decision_dict['_etag']
        # del decision_dict['kind']
        # del decision_dict['_created']
        # del decision_dict['_links']
        # return Decision.from_dict(decision_dict)
        decision = Decision(
            _id=decision_dict['_id'],
            serviceName=decision_dict['serviceName'],
            projectId=decision_dict['projectId'],
            account=Account(
                email=decision_dict['account']['email']
            ) if 'account' in decision_dict.keys() else None,
            device=Device(
                fingerprint=decision_dict['device']['fingerprint']
            ) if 'device' in decision_dict.keys() else None,
            rule=Rule(
                isBlocked=decision_dict['rule']['isBlocked'],
                exceptions=decision_dict['rule']['exceptions']
            ),
            automaticDecisionDetail=AutomaticDecisionDetail(
                isManualDecisionRequired=decision_dict['automaticDecisionDetail']['isManualDecisionRequired'],
                score=decision_dict['automaticDecisionDetail']['score'],
                signals=decision_dict['automaticDecisionDetail']['signals'],
                makeFormDecisionId=decision_dict['automaticDecisionDetail']['makeFormDecisionId'],
                fraudServicesId=decision_dict['automaticDecisionDetail']['fraudServicesId']
            ))
        logger.info(f'decision get result: {decision.account.email if account else decision.device.fingerprint}, decision={decision}')
        return decision

    def get_fraudservice(self, project_id, service_name):
        url = f'{self.url}/projects/{project_id}/fraudServices/{service_name}?sort=-_created&max_results=1'
        # Process the request
        logger.debug(f"getting fraudService... url={url}")
        response = requests.get(url)
        response.raise_for_status()
        logger.info(f'response{response.json()}')
        try:
            config_list = response.json()['_items']
        except:
            config_list = response.json() #current format

        logger.info(f'config_list:{config_list}')
        if len(config_list)==0:
            logger.info(f'fraudService get result: no fraudService found. url={url}')
            return None

        try:
            fraudService = FraudService.from_dict(config_list[-1])
            logger.info(f'fraudService : {fraudService}')
        except:
            fraudService = FraudService.from_dict(config_list) #current format
            logger.info(f'fraudService : {fraudService}')
        return fraudService
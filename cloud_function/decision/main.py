from google.cloud import bigquery
import json
import base64
import os

from decision.api import *
from decision.decision_maker import *
from decision.score_calculator import *
from utils.logger import logger
from utils.dataclasses.make_form_decision import *
from google.oauth2 import service_account

def get_last_device_for_email(email,GOOGLE_PROJECT_ID,BIGQUERY_DATASET_NAME,BIGQUERY_TABLE_NAME):
    KEY_PATH= "../../key.json"
    credentials = service_account.Credentials.from_service_account_file(
        KEY_PATH, scopes=["https://www.googleapis.com/auth/cloud-platform"],
     )

    client =bigquery.Client(credentials=credentials,project=credentials.project_id)
    # query for getting last device fingerprint for this account
    query = f"""
                SELECT data.session.fingerprint
                FROM `{GOOGLE_PROJECT_ID}.{BIGQUERY_DATASET_NAME}.{BIGQUERY_TABLE_NAME}`, unnest(data.FormInteraction.formfields) as d 
                where 
                        d.content like "{email}"
                ORDER BY data.requestDate DESC
                LIMIT 1;
            """
    try:
        query_job = client.query(query)
        data = query_job.result()
        rows = list(data)
        if not len(rows) == 0:
            logger.info(f"Last fingerprint for {email} : {rows[0]['fingerprint']}")
            print(rows[0]['fingerprint'])
            return rows[0]['fingerprint']
        else:
            logger.info(f"There is no device for {email} in bigquery")
            return None
    except Exception as e:
        logger.info('Failed to get last device fingerprint:'+str(e))
        return None

def decision_function(event, context):

    logger.debug(f'decision function called.')

    API_URL = os.getenv('API_URL')
    BIGQUERY_DATASET_NAME = os.getenv('BIGQUERY_DATASET_NAME')
    BIGQUERY_TABLE_NAME = os.getenv('BIGQUERY_TABLE_NAME')
    GOOGLE_PROJECT_ID = os.getenv('GOOGLE_PROJECT_ID')

    api = API(API_URL)

    event_data = event['data']
    try:
        event_data = base64.b64decode(event_data).decode('utf-8')
    except Exception as e:
        logger.warning(e)

    make_form_decision_dict = json.loads(event_data)['data']
    make_form_decision = MakeFormDecision.from_dict(make_form_decision_dict)

    logger.info(f'decision function event loaded. make_form_decision={make_form_decision_dict}')

    #TODO: backward compatibility
    if 'email' in make_form_decision_dict.keys():
        make_form_decision.account = Account(email=make_form_decision_dict['email'])
    if 'fingerprint' in make_form_decision_dict.keys():
        make_form_decision.device = Device(fingerprint=make_form_decision_dict['fingerprint'])
    make_form_decision.isBlocked = None
    # if make_form_decision.serviceName is None:
    make_form_decision.serviceName = "opening"
    
    make_form_decision.projectId = api.get_projectId_by_siteId(make_form_decision.siteId)
    if not make_form_decision.projectId:
        logger.debug(f'Project Id missed')
        make_form_decision.projectId = "62274ab07881f1715a512db6" #TODO: comment this
        #return

    logger.debug(f'edited make_form_decision={make_form_decision_dict}')

    # if not make_form_decision.account or not make_form_decision.device:
    #     logger.info(
    #         f"return from decision function: {'device' if not make_form_decision.device else 'account'} is null")
    #     return
    # if not make_form_decision.account.email or not make_form_decision.device.fingerprint:
    #     logger.info(
    #         f"return from decision function: {'fingerprint' if not make_form_decision.device.fingerprint else 'email'} is null")
    #     return

    if not make_form_decision.account or not make_form_decision.account.email:
        return

    if not make_form_decision.device or not make_form_decision.device.fingerprint:
        logger.info("fingerprint is null .... Getting last fingerprint which this account used")
        make_form_decision.device.fingerprint = get_last_device_for_email(make_form_decision.account.email,GOOGLE_PROJECT_ID,BIGQUERY_DATASET_NAME,BIGQUERY_TABLE_NAME)
        logger.info(f"make_form_decision.device.fingerprint {make_form_decision.device.fingerprint}")
        print (f"make_form_decision.device.fingerprint {make_form_decision.device.fingerprint}")

    make_form_decision:MakeFormDecision = api.post_make_form_decision(make_form_decision)

    last_decision_for_account = api.get_decision(make_form_decision.projectId, make_form_decision.serviceName, account=make_form_decision.account)
    last_decision_for_device = api.get_decision(make_form_decision.projectId, make_form_decision.serviceName, device=make_form_decision.device)
    is_blocked_based_on_last_decisions = DecisionMaker.is_form_blocked(
        decision_for_account=last_decision_for_account, 
        decision_for_device=last_decision_for_device, 
        account=make_form_decision.account, 
        device=make_form_decision.device)

    logger.info(f"the form is {'' if is_blocked_based_on_last_decisions else 'NOT'} blocked based on last decisions")

    if not is_blocked_based_on_last_decisions:

        siteId = int(make_form_decision.siteId)
        email = make_form_decision.account.email
        logger.debug('calculating score...')
        scoreCalculator = ScoreCalculator(
            email=email, 
            siteId=siteId,
            google_project_id=GOOGLE_PROJECT_ID,
            dataset_name=BIGQUERY_DATASET_NAME,
            table_name=BIGQUERY_TABLE_NAME)
        score, signals = scoreCalculator.calcluate_score_and_signals()
        logger.info(f'score: {score}, signals: {signals}')
        fraudservice = api.get_fraudservice(make_form_decision.projectId, make_form_decision.serviceName)
        decisionMaker = DecisionMaker(
            score=score,
            signals=signals,
            past_connected_accounts=[], # TODO
            past_connected_devices=[],
            makeFormDecision=make_form_decision,
            config=fraudservice)
        decision_for_account, decision_for_device = decisionMaker.decide()

        decision_for_account = api.post_decision(decision_for_account)
        decision_for_device = api.post_decision(decision_for_device)
        
        is_blocked = DecisionMaker.is_form_blocked(
            decision_for_account=decision_for_account,
            decision_for_device=decision_for_device,
            account=make_form_decision.account,
            device=make_form_decision.device)
        logger.info(f"the form is {'' if is_blocked else 'NOT'} blocked based on current decisions")
    else:
        decision_for_account = last_decision_for_account
        decision_for_device = last_decision_for_device
        is_blocked = is_blocked_based_on_last_decisions

        # TODO: POST decision if the account or the device is unseen

        #  if the account has been created by blocked device
        if make_form_decision.serviceName == ServiceName.OPENING:
            decision_for_account = DecisionMaker.decision_for_new_accounts_on_blocked_device(decision_for_device,decision_for_account,make_form_decision.account,GOOGLE_PROJECT_ID)
            decision_for_account = api.post_decision(decision_for_account)

        #  if blocked account logged in with a new device
        if make_form_decision.serviceName == ServiceName.TAKEOVER:
            decision_for_device = DecisionMaker.decision_for_new_device_with_blocked_account(decision_for_device,decision_for_account,make_form_decision.device,make_form_decision.account,GOOGLE_PROJECT_ID)
            decision_for_device = api.post_decision(decision_for_device)
    
    make_form_decision.isBlocked = is_blocked
    make_form_decision.decisionDetails = decisionDetails(
        decisionOnAccount=decision_for_account._id if decision_for_account else None,
        decisionOnDevice= decision_for_device._id if decision_for_device else None
    )
    api.patch_make_form_decision(make_form_decision)

import json

import requests
from requests import *
class FreeProviderChecker ():
    def is_in_mini_free_email_providers_list(self, emailDomain):
        f = open('decision/free_domain_provider_detector/freeProviders')
        for line in f:
            line = line.strip('\n')
            if line == emailDomain:
                f.close()
                return True
        f.close()
        return False

    def is_in_complete_free_email_providers_list(self, emailDomain):
        f = open('decision/free_domain_provider_detector/freeProviders_completedList')
        for line in f:
            line = line.strip('\n')
            if line == emailDomain:
                f.close()
                return True
        f.close()
        return False

    def is_in_api_layer_free_list(self, emailAddress): #  this is the last step for checking the domain by using an api
        try:
            header = {'apikey':'8DMISf5vViBYmeCer2qW6DQoEvih9knF'} #constant apikey
            response = requests.get("https://api.apilayer.com/email_verification/" + emailAddress,headers=header)
            info = json.loads(response.text)
            IsFree = info['free']
            if IsFree :
                return True
            else:
                return False
        except:
            # to many requests for apilayer api for today(more than 50) ...
            return False

    def find_domain_of_email(self, emailAddress):
        emailParts = emailAddress.split("@")
        domain = emailParts[1]
        # print(domain)
        return domain


    def is_free_email_provider(self, emailAddress):
        domain = self.find_domain_of_email(emailAddress)

        #checking in mini list
        isFree = self.is_in_mini_free_email_providers_list(domain)
        if isFree:
            return True

        #checking with complete list
        isFree = self.is_in_complete_free_email_providers_list(domain)
        if isFree:
            return True

        #checking with an API
        try:
            isFree = self.is_in_api_layer_free_list(emailAddress)
            if isFree:
                return True
            else:
                return False
        except Exception as e:
            return False

    def is_provider_free(self, emailAddress):
        if self.is_free_email_provider(emailAddress):
            return 1
        else:
            return 0


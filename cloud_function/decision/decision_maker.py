from copy import deepcopy
from utils.dataclasses.make_form_decision import *
from utils.dataclasses.fraud_services import *
from utils.dataclasses.decision import *
from utils.enums.restriction_policy import *
from utils.enums.service_name import *



class DecisionMaker:
    def __init__(self, score, signals, 
                past_connected_accounts, past_connected_devices,
                makeFormDecision:MakeFormDecision, config:FraudService) -> None:
        self.score = score
        self.signals = signals
        self.past_connected_accounts = past_connected_accounts
        self.past_connected_devices = past_connected_devices
        self.makeFormDecision = makeFormDecision
        self.config = config

    def decide(self):

        decision = Decision(
            serviceName=self.config.serviceName,
            projectId=self.config.projectId,
            automaticDecisionDetail=AutomaticDecisionDetail(
                isManualDecisionRequired=
                    self.config.manualDecisionRequiredAt.fromScore
                    < self.score <
                    self.config.manualDecisionRequiredAt.toScore,
                score=self.score,
                signals=self.signals,
                makeFormDecisionId=self.makeFormDecision._id,
                fraudServicesId=self.config._id))

        is_blocked = (self.score >= self.config.automaticDecision.blockFromScore)

        decision_for_account = deepcopy(decision)
        decision_for_account.rule\
            = DecisionMaker.create_rule(
                is_blocked=is_blocked,
                past_connected_entities=self.past_connected_devices,
                config_restriction_policy=self.config.automaticDecision.accountRestrictionPolicy,
                last_connected_entity=self.makeFormDecision.device)
        
        decision_for_account.account=self.makeFormDecision.account
        decision_for_device = deepcopy(decision)
        decision_for_device.rule\
            = DecisionMaker.create_rule(
                is_blocked=is_blocked,
                past_connected_entities=self.past_connected_accounts,
                config_restriction_policy=self.config.automaticDecision.deviceRestrictionPolicy,
                last_connected_entity=self.makeFormDecision.account)
        decision_for_device.device=self.makeFormDecision.device

        

        return decision_for_account, decision_for_device

            
    def create_rule(is_blocked, past_connected_entities, last_connected_entity, config_restriction_policy):
        if not is_blocked:
            return Rule(isBlocked=False, exceptions=[])
        
        if RestrictionPolicyEnum.BLOCK_CURRENT in config_restriction_policy:
            return Rule(isBlocked=False, exceptions=[last_connected_entity])

        if RestrictionPolicyEnum.BLOCK_FUTURE in config_restriction_policy:
            return Rule(isBlocked=True, exceptions=past_connected_entities)

        if RestrictionPolicyEnum.BLOCK_ALL in config_restriction_policy:
            return Rule(isBlocked=True, exceptions=[])

    def is_blocked_entity_based_on_decision(rule:Rule, connected_entity):
        is_blocked = rule.isBlocked
        if is_blocked==None:
            is_blocked = False
        if connected_entity in rule.exceptions:
            is_blocked = not is_blocked
        return is_blocked

    def is_form_blocked(decision_for_account:Decision, decision_for_device:Decision, device, account):
        is_account_blocked = DecisionMaker.is_blocked_entity_based_on_decision(decision_for_account.rule, device) \
            if decision_for_account else False
        is_device_blocked = DecisionMaker.is_blocked_entity_based_on_decision(decision_for_device.rule, account) \
            if decision_for_device else False
        return (is_account_blocked or is_device_blocked)

    def decision_for_new_accounts_on_blocked_device(decision_for_device:Decision,decision_for_account:Decision,account:Account,projectID):
        if decision_for_device.serviceName == ServiceName.OPENING and decision_for_device.rule.isBlocked == True: 
            if decision_for_device.rule.exceptions is None or not (account in decision_for_device.rule.exceptions):#The device was blocked from opening any account or future account
                if decision_for_account is None:
                    decision_for_account = Decision(
                    account= Account(account.email),
                    serviceName=ServiceName.TAKEOVER,# the fraud account can not login
                    projectId=projectID)
                    decision_for_account.rule = Rule(isBlocked = True,exceptions=[])
                    return decision_for_account
        return decision_for_account

    def decision_for_new_device_with_blocked_account(decision_for_device:Decision,decision_for_account:Decision,device:Device,account:Account,projectID):
        if decision_for_device.serviceName == ServiceName.TAKEOVER and decision_for_account.rule.isBlocked == True:
            if decision_for_account.rule.exceptions is None or not(device in decision_for_device.rule.exceptions):
                if decision_for_device is None:
                    decision_for_device = Decision(
                        device = device.fingerprint,
                        serviceName= ServiceName.TAKEOVER,
                        projectId=projectID
                    )
                    decision_for_device.rule = Rule(isBlocked=False , exceptions = [account.email]) #TODO: or Rule(isBlocked=True , exceptions = [])
                    return decision_for_device
        return decision_for_device


import math
import statistics
from utils.enums.fraud_features import FF
from google.cloud import bigquery
from time import sleep
from decision.free_domain_provider_detector.free_email_provider_checker import FreeProviderChecker

class ScoreCalculator:

    normalizer_param = {
        FF.NUM_LOGIN_ATTEMPTS_ON_EMAIL: 2,
        FF.NUM_SIGNUP_ATTEMPTS_ON_EMAIL: 1,
        FF.NUM_CITIES_ON_EMAIL: 2,
        FF.NUM_IP_ON_EMAIL: 5,
        FF.NUM_COUNTRIES_ON_EMAIL: 1,
        FF.NUM_DEVICES_ON_EMAIL: 2,
        FF.NUM_OS_ON_EMAIL: 2,
        FF.NUM_EMAIL_ON_IPS_ON_EMAIL: 1,
        FF.NUM_EMAIL_ON_IP: 1,
        FF.FORM_TIME_SPENT: 30000, # more than 30 second
        FF.IS_EMAIL_DOMAIN_PROVIDER_FREE: 1,
    }

    signal_threshold = {
        FF.NUM_IP_ON_EMAIL: 0.5,
        FF.NUM_LOGIN_ATTEMPTS_ON_EMAIL: 0.5
    }

    signal_map = {
        FF.NUM_IP_ON_EMAIL: 'multiple IP on account',
        FF.NUM_LOGIN_ATTEMPTS_ON_EMAIL: 'faild login'
    }

    def __init__(self, email, siteId, google_project_id, dataset_name, table_name) -> None:
        self.email = email
        self.siteId = siteId
        self.dataset_name = dataset_name
        self.table_name = table_name
        self.google_project_id = google_project_id

    def calcluate_score_and_signals(self):
        raw_values = self.calculate_raw_values()
        normalized_values = ScoreCalculator.calculate_normalized_values(raw_values)
        score = int(statistics.mean(list(normalized_values.values())))
        signals = ScoreCalculator.get_signals(normalized_values)
        return score, signals

    def calculate_raw_values(self):

        raw_values = {}
        id_to_name = {}
        awaiting_jobs = set()

        self.client = bigquery.Client()

        def callback(future):
            awaiting_jobs.discard(future.job_id)
            for row in future:
                raw_values[id_to_name[future.job_id]] = row['rslt']

        for name, query in self.get_queries().items():
            job = self.client.query(query)
            id_to_name[job.job_id] = name
            awaiting_jobs.add(job.job_id)
            job.add_done_callback(callback)

        while awaiting_jobs:
            # print('waiting for jobs to finish ... sleeping for 1s')
            sleep(1)

        # print('all jobs done, do your stuff')
        # adding the external feature ...
        for name, value in self.add_email_external_source_features().items():
            raw_values[name] = value

        return raw_values

    def calculate_normalized_values(raw_values):
        return {
            name: ScoreCalculator.normalize(raw_value, ScoreCalculator.normalizer_param[name])
            for name, raw_value in raw_values.items()
            }

    def get_signals(normalized_values):
        signals = set()
        for name, threshold in ScoreCalculator.signal_threshold.items():
            if normalized_values[name] >= threshold:
                signals.add(ScoreCalculator.signal_map[name])
        return list(signals)

    def sigmoid(x):
        sig = 1 / (1 + math.exp(-x))
        return sig

    def normalize(x, q):
        return int(((2*(ScoreCalculator.sigmoid(x/q)))-1) * 100)

    def add_email_external_source_features(self):
        fpc = FreeProviderChecker()
        return {
            # is email domain free ...
            FF.IS_EMAIL_DOMAIN_PROVIDER_FREE:
                fpc.is_provider_free(emailAddress=self.email),

            # TODO:FF.DataBreaches and FF.emailAge...
        }

    def get_queries(self):
        return {
            FF.NUM_IP_ON_EMAIL:
                f"""
                    SELECT count(distinct(data.session.ipLocation.ip)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                    LIMIT 1;
                """,
            FF.NUM_LOGIN_ATTEMPTS_ON_EMAIL:
                f"""
                    SELECT count(distinct(data.FormInteraction.formview.formviewid)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            data.FormInteraction.isSubmit = true
                            and contains_substr(data.FormInteraction.formview.formName, "login")
                            and cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                    LIMIT 1;
                """,
            FF.NUM_CITIES_ON_EMAIL: 
                f"""
                    SELECT count(distinct(data.session.ipLocation.city)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                    LIMIT 1000
                """,
            FF.NUM_EMAIL_ON_IPS_ON_EMAIL:
                f"""
                    DECLARE ips_of_this_email ARRAY<STRING>;

                    SET ips_of_this_email = (
                            SELECT ARRAY_AGG(distinct(data.session.ipLocation.ip))
                            FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                            where 
                                    cast(data.session.siteId as int64) = {self.siteId}
                                    and contains_substr(d.name, 'email')
                                    and d.content like "{self.email}"
                            LIMIT 1000);

                    select count(distinct(d.content)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where data.session.ipLocation.ip in unnest(ips_of_this_email)
                                    and cast(data.session.siteId as int64) = {self.siteId}
                                    and contains_substr(d.name, 'email');
                """,
            FF.NUM_EMAIL_ON_IP: 
                f"""
                    select count(distinct(d.content)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where data.session.ipLocation.ip = (SELECT data.session.ipLocation.ip
                                                            FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                                                            where cast(data.session.siteId as int64) = {self.siteId}
                                                            and contains_substr(d.name, 'email')
                                                            and d.content like "{self.email}"
                                                            order by data.parseTime desc
                                                            limit 1)
                            and cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email');
                """,
            FF.NUM_COUNTRIES_ON_EMAIL: 
                f"""
                    SELECT count(distinct(data.session.ipLocation.country)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                    LIMIT 1000;
                """,
            FF.NUM_DEVICES_ON_EMAIL: 
                f"""
                    SELECT count(distinct(data.session.userAgent.device.model)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                    LIMIT 1000;
                """,
            FF.NUM_OS_ON_EMAIL:
                f"""
                    SELECT count(distinct(data.session.userAgent.os.name)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                    LIMIT 1000;
                """,
            FF.FORM_TIME_SPENT: 
                f"""
                    SELECT data.FormInteraction.timeSpent as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            data.FormInteraction.isSubmit = true
                            and cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                    order by data.parseTime desc
                    LIMIT 1;
                """,
            FF.NUM_SIGNUP_ATTEMPTS_ON_EMAIL: 
                f"""
                    SELECT count(distinct(data.FormInteraction.formview.formviewid)) as rslt
                    FROM `{self.google_project_id}.{self.dataset_name}.{self.table_name}`, unnest(data.FormInteraction.formfields) as d 
                    where 
                            data.FormInteraction.isSubmit = true
                            and contains_substr(data.FormInteraction.formview.formName, "signup")
                            and cast(data.session.siteId as int64) = {self.siteId}
                            and contains_substr(d.name, 'email')
                            and d.content like "{self.email}"
                """
        }
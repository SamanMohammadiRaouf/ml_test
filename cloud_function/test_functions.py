import os
from configparser import ConfigParser

config = ConfigParser()
config.read('config.ini')

os.environ['GENERAL_LOG_LEVEL']                 = config.get('general', 'GENERAL_LOG_LEVEL')
os.environ['GENERAL_LOG_TO_CLOUD']              = config.get('general', 'GENERAL_LOG_TO_CLOUD')
if 'GOOGLE_APPLICATION_CREDENTIALS' not in os.environ.keys():
    os.environ['GOOGLE_APPLICATION_CREDENTIALS']= config.get('google',  'GOOGLE_APPLICATION_CREDENTIALS')
os.environ['GOOGLE_PROJECT_ID']                 = config.get('google',  'GOOGLE_PROJECT_ID')
os.environ['PUBSUB_DECISION_TRIGGER']           = config.get('pubsub',  'PUBSUB_DECISION_TRIGGER')
os.environ['PUBSUB_CC_RAW_DATA']                = config.get('pubsub',  'PUBSUB_CC_RAW_DATA')
os.environ['BIGQUERY_DATASET_NAME']             = config.get('bigquery','BIGQUERY_DATASET_NAME')
os.environ['BIGQUERY_TABLE_NAME']               = config.get('bigquery','BIGQUERY_TABLE_NAME')
os.environ['API_URL']                           = config.get('api',     'API_URL')

GOOGLE_PROJECT_ID       = config.get('google', 'GOOGLE_PROJECT_ID')
PUBSUB_CC_RAW_DATA      = config.get('pubsub', 'PUBSUB_CC_RAW_DATA')
PUBSUB_DECISION_TRIGGER = config.get('pubsub', 'PUBSUB_DECISION_TRIGGER')
PUBSUB_MATOMO_RAW_DATA  = config.get('pubsub', 'PUBSUB_MATOMO_RAW_DATA')


import json

from ingestion.main import ingestion_function
from activator.main import activator_function
from decision.main import decision_function
from ps2bq.main import ps2bq_function

def run(topic_name, function):
    with open(f'messages/{topic_name}.txt', "r") as f:
        lines = f.readlines()
        for line in lines:
            message = json.loads(line)
            function({'data': f"{message['data']}", 'attributes': dict(message['attributes'])}, None)
    assert True


def test_ingestion():
    run('matomo_raw_data', ingestion_function)

def test_ps2bq():
    run('cc_raw_data', ps2bq_function)

def test_activator():
    run('cc_raw_data', activator_function)

def test_decision():
    run('decision_trigger', decision_function)

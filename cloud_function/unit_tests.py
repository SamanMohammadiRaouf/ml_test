import unittest
# free email provider tests ...
from decision.free_domain_provider_detector.free_email_provider_checker import FreeProviderChecker

class SimpleTest(unittest.TestCase):

    def test_free_provider(self):
        checker = FreeProviderChecker()
        IsFree = checker.is_free_email_provider("saman.raouf@gmail.com")
        print(IsFree)
        self.assertTrue(IsFree)

    def test_free_provider2(self):
        checker = FreeProviderChecker()
        Isfree = checker.is_free_email_provider('saman.raouf@samanraf.com')
        print(Isfree)
        self.assertFalse(Isfree)

    def test_free_providerd3(self):
        checker = FreeProviderChecker()
        IsFree = checker.is_free_email_provider('saman.raouf@yahoo.com')
        print(IsFree)
        self.assertTrue(IsFree)


    def test_free_provider4(self):
        checker = FreeProviderChecker()
        IsFree = checker.is_free_email_provider('customercare@apilayer.com')
        print(IsFree)
        self.assertFalse(IsFree)
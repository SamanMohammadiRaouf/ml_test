import os
from configparser import ConfigParser
config = ConfigParser()
config.read('config.ini')

os.environ['GOOGLE_APPLICATION_CREDENTIALS']    = config.get('google',  'GOOGLE_APPLICATION_CREDENTIALS')

###################################

from google.cloud import logging
from datetime import datetime, timedelta

client = logging.Client()

time = datetime.utcnow() + timedelta(minutes=-1)
time_str = time.strftime("%Y-%m-%dT%H:%M:%S%Z")
FILTER = f'timestamp >= "{time_str}"'
"2022-01-20T00:00:00Z"
entries = client.list_entries(filter_=FILTER)  # API call

for entry in entries:
    timestamp = entry.timestamp.isoformat()
    print('%sZ: %s' %(timestamp, entry.payload))